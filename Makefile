CXX = g++
CXXFLAGS = -g-Wall-Wexta-Wpedantic

numberconversion : romandigitconrter.cpp numerconversion.o
	$(CXX) $(CXXFLAGS) -o numberconversion romandigitconrter.cpp



.PHONY : clean
clean :
	$(RM) *.o numberconversion
